-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: jplus
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jp_dept`
--

DROP TABLE IF EXISTS `jp_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父部门id',
  `pids` varchar(255) DEFAULT NULL COMMENT '父级ids',
  `simplename` varchar(45) DEFAULT NULL COMMENT '简称',
  `fullname` varchar(255) DEFAULT NULL COMMENT '全称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='部门表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_dept`
--

LOCK TABLES `jp_dept` WRITE;
/*!40000 ALTER TABLE `jp_dept` DISABLE KEYS */;
INSERT INTO `jp_dept` VALUES (24,1,0,'[0],','总公司','总公司','',NULL),(25,2,24,'[0],[24],','开发部','开发部','',NULL),(26,3,24,'[0],[24],','运营部','运营部','测试',NULL);
/*!40000 ALTER TABLE `jp_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_dict`
--

DROP TABLE IF EXISTS `jp_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='字典表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_dict`
--

LOCK TABLES `jp_dict` WRITE;
/*!40000 ALTER TABLE `jp_dict` DISABLE KEYS */;
INSERT INTO `jp_dict` VALUES (16,0,0,'状态',NULL),(17,1,16,'启用',NULL),(18,2,16,'禁用',NULL),(29,0,0,'性别',NULL),(30,1,29,'男',NULL),(31,2,29,'女',NULL),(35,0,0,'账号状态',NULL),(36,1,35,'启用',NULL),(37,2,35,'冻结',NULL),(38,3,35,'已删除',NULL);
/*!40000 ALTER TABLE `jp_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_log`
--

DROP TABLE IF EXISTS `jp_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logtype` varchar(255) DEFAULT NULL COMMENT '日志类型:1、登录日志，2、业务日志，3、异常日志',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '用户id',
  `classname` varchar(255) DEFAULT NULL COMMENT '类名称',
  `method` text COMMENT '方法名称',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否成功',
  `message` text COMMENT '备注',
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=483 DEFAULT CHARSET=utf8 COMMENT='操作日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_log`
--

LOCK TABLES `jp_log` WRITE;
/*!40000 ALTER TABLE `jp_log` DISABLE KEYS */;
INSERT INTO `jp_log` VALUES (480,'业务日志','清空业务日志',1,'com.stylefeng.guns.modular.system.controller.LogController','delLog','2017-06-03 23:04:22','成功','主键id=null',NULL),(481,'业务日志','清空登录日志',1,'com.stylefeng.guns.modular.system.controller.LoginLogController','delLog','2017-06-03 23:04:25','成功','主键id=null',NULL),(482,'业务日志','修改菜单',1,'com.stylefeng.guns.modular.system.controller.MenuController','edit','2017-06-04 10:22:58','成功','菜单名称=分配角色跳转;;;字段名称:url地址,旧值:/role/role_assign,新值:/mgr/role_assign',NULL);
/*!40000 ALTER TABLE `jp_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_menu`
--

DROP TABLE IF EXISTS `jp_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_menu` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `pcode` varchar(255) DEFAULT NULL COMMENT '菜单父编号',
  `pcodes` varchar(255) DEFAULT NULL COMMENT '当前菜单的所有父菜单编号',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `num` int(65) DEFAULT NULL COMMENT '菜单排序号',
  `levels` int(65) DEFAULT NULL COMMENT '菜单层级',
  `ismenu` int(11) DEFAULT NULL COMMENT '是否是菜单（1：是  0：不是）',
  `tips` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(65) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  `isopen` int(11) DEFAULT NULL COMMENT '是否打开:    1:打开   0:不打开',
  `pid` int(65) DEFAULT NULL COMMENT '父编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COMMENT='菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_menu`
--

LOCK TABLES `jp_menu` WRITE;
/*!40000 ALTER TABLE `jp_menu` DISABLE KEYS */;
INSERT INTO `jp_menu` VALUES (105,'system','0','[0],','系统管理','fa-user','',3,1,1,NULL,1,1,0),(106,'mgr','system','[0],[system],','用户管理','','/admin/user',1,2,1,NULL,1,0,105),(107,'mgr_add','mgr','[0],[system],[mgr],','添加用户',NULL,'/admin/user/add',1,3,0,NULL,1,0,106),(108,'mgr_edit','mgr','[0],[system],[mgr],','修改用户',NULL,'/admin/user/edit',2,3,0,NULL,1,0,106),(109,'mgr_delete','mgr','[0],[system],[mgr],','删除用户',NULL,'/admin/user/delete',3,3,0,NULL,1,0,106),(110,'mgr_reset','mgr','[0],[system],[mgr],','重置密码',NULL,'/admin/mgr/reset',4,3,0,NULL,1,0,106),(111,'mgr_freeze','mgr','[0],[system],[mgr],','冻结用户',NULL,'/admin/mgr/freeze',5,3,0,NULL,1,0,106),(112,'mgr_unfreeze','mgr','[0],[system],[mgr],','解除冻结用户',NULL,'/adinm/mgr/unfreeze',6,3,0,NULL,1,0,106),(113,'mgr_setRole','mgr','[0],[system],[mgr],','分配角色',NULL,'/admin/mgr/setRole',7,3,0,NULL,1,0,106),(114,'role','system','[0],[system],','角色管理',NULL,'/admin/role',2,2,1,NULL,1,0,105),(115,'role_add','role','[0],[system],[role],','添加角色',NULL,'/admin/role/add',1,3,0,NULL,1,0,114),(116,'role_edit','role','[0],[system],[role],','修改角色',NULL,'/admin/role/edit',2,3,0,NULL,1,0,114),(117,'role_remove','role','[0],[system],[role],','删除角色',NULL,'/admin/role/remove',3,3,0,NULL,1,0,114),(118,'role_setAuthority','role','[0],[system],[role],','配置权限',NULL,'/admin/role/setAuthority',4,3,0,NULL,1,0,114),(119,'menu','system','[0],[system],','菜单管理',NULL,'/admin/menu',4,2,1,NULL,1,0,105),(120,'menu_add','menu','[0],[system],[menu],','添加菜单',NULL,'/admin/menu/add',1,3,0,NULL,1,0,119),(121,'menu_edit','menu','[0],[system],[menu],','修改菜单',NULL,'/admin/menu/edit',2,3,0,NULL,1,0,119),(122,'menu_remove','menu','[0],[system],[menu],','删除菜单',NULL,'/admin/menu/remove',3,3,0,NULL,1,0,119),(128,'log','system','[0],[system],','业务日志',NULL,'/admin/log',6,2,1,NULL,1,0,105),(131,'dept','system','[0],[system],','部门管理',NULL,'/admin/dept',3,2,1,NULL,1,NULL,105),(132,'dict','system','[0],[system],','字典管理',NULL,'/admin/dict',4,2,1,NULL,1,NULL,105),(133,'loginLog','system','[0],[system],','登录日志',NULL,'/admin/loginLog',6,2,1,NULL,1,NULL,105),(134,'log_clean','log','[0],[system],[log],','清空日志',NULL,'/admin/log/delLog',3,3,0,NULL,1,NULL,128),(135,'dept_add','dept','[0],[system],[dept],','添加部门',NULL,'/admin/dept/add',1,3,0,NULL,1,NULL,131),(136,'dept_update','dept','[0],[system],[dept],','修改部门',NULL,'/admin/dept/update',1,3,0,NULL,1,NULL,131),(137,'dept_delete','dept','[0],[system],[dept],','删除部门',NULL,'/admin/dept/delete',1,3,0,NULL,1,NULL,131),(138,'dict_add','dict','[0],[system],[dict],','添加字典',NULL,'/admin/dict/add',1,3,0,NULL,1,NULL,132),(139,'dict_update','dict','[0],[system],[dict],','修改字典',NULL,'/admin/dict/update',1,3,0,NULL,1,NULL,132),(140,'dict_delete','dict','[0],[system],[dict],','删除字典',NULL,'/admin/dict/delete',1,3,0,NULL,1,NULL,132),(141,'notice','system','[0],[system],','通知管理',NULL,'/admin/notice',9,2,1,NULL,1,NULL,105),(142,'notice_add','notice','[0],[system],[notice],','添加通知',NULL,'/admin/notice/add',1,3,0,NULL,1,NULL,141),(143,'notice_update','notice','[0],[system],[notice],','修改通知',NULL,'/admin/notice/update',2,3,0,NULL,1,NULL,141),(144,'notice_delete','notice','[0],[system],[notice],','删除通知',NULL,'/admin/notice/delete',3,3,0,NULL,1,NULL,141),(145,'hello','0','[0],','通知','fa-rocket','/admin/notice/hello',1,1,1,NULL,1,NULL,0),(148,'code','system','[0],[system],','代码生成','fa-user','/admin/code',10,2,1,NULL,1,NULL,105),(149,'api_mgr','0','[0],','接口文档','fa-leaf','/swagger-ui.html',2,1,1,NULL,1,NULL,0),(150,'to_menu_edit','menu','[0],[system],[menu],','菜单编辑跳转','','/admin/menu/menu_edit',4,3,0,NULL,1,NULL,119),(151,'menu_list','menu','[0],[system],[menu],','菜单列表','','/admin/menu/list',5,3,0,NULL,1,NULL,119),(152,'to_dept_update','dept','[0],[system],[dept],','修改部门跳转','','/admin/dept/dept_update',4,3,0,NULL,1,NULL,131),(153,'dept_list','dept','[0],[system],[dept],','部门列表','','/admin/dept/list',5,3,0,NULL,1,NULL,131),(154,'dept_detail','dept','[0],[system],[dept],','部门详情','','/admin/dept/detail',6,3,0,NULL,1,NULL,131),(155,'to_dict_edit','dict','[0],[system],[dict],','修改菜单跳转','','/admin/dict/dict_edit',4,3,0,NULL,1,NULL,132),(156,'dict_list','dict','[0],[system],[dict],','字典列表','','/admin/dict/list',5,3,0,NULL,1,NULL,132),(157,'dict_detail','dict','[0],[system],[dict],','字典详情','','/admin/dict/detail',6,3,0,NULL,1,NULL,132),(158,'log_list','log','[0],[system],[log],','日志列表','','/admin/log/list',2,3,0,NULL,1,NULL,128),(159,'log_detail','log','[0],[system],[log],','日志详情','','/admin/log/detail',3,3,0,NULL,1,NULL,128),(160,'del_login_log','loginLog','[0],[system],[loginLog],','清空登录日志','','/admin/loginLog/delLoginLog',1,3,0,NULL,1,NULL,133),(161,'login_log_list','loginLog','[0],[system],[loginLog],','登录日志列表','','/admin/loginLog/list',2,3,0,NULL,1,NULL,133),(162,'to_role_edit','role','[0],[system],[role],','修改角色跳转','','/admin/role/role_edit',5,3,0,NULL,1,NULL,114),(163,'to_role_assign','role','[0],[system],[role],','角色分配跳转','','/admin/role/role_assign',6,3,0,NULL,1,NULL,114),(164,'role_list','role','[0],[system],[role],','角色列表','','/admin/role/list',7,3,0,NULL,1,NULL,114),(165,'to_assign_role','mgr','[0],[system],[mgr],','分配角色跳转','','/admin/mgr/role_assign',8,3,0,NULL,1,NULL,106),(166,'to_user_edit','mgr','[0],[system],[mgr],','编辑用户跳转','','/admin/mgr/user_edit',9,3,0,NULL,1,NULL,106),(167,'mgr_list','mgr','[0],[system],[mgr],','用户列表','','/admin/mgr/list',10,3,0,NULL,1,NULL,106),(168,'monitor','0',NULL,'监控管理','fa-rocket',NULL,2,NULL,1,NULL,1,NULL,0),(169,'monitor-druid','168',NULL,'数据源监控',NULL,'http://localhost:8099/admin/druid',1,NULL,1,NULL,1,NULL,168),(171,'test',NULL,NULL,'测试',NULL,'/admin/test',2,NULL,1,NULL,1,NULL,168);
/*!40000 ALTER TABLE `jp_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_notice`
--

DROP TABLE IF EXISTS `jp_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='通知表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_notice`
--

LOCK TABLES `jp_notice` WRITE;
/*!40000 ALTER TABLE `jp_notice` DISABLE KEYS */;
INSERT INTO `jp_notice` VALUES (6,'世界',10,'欢迎使用Guns管理系统','2017-01-11 08:53:20',1),(8,'你好',NULL,'你好','2017-05-10 19:28:57',1);
/*!40000 ALTER TABLE `jp_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_option`
--

DROP TABLE IF EXISTS `jp_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_option` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `option_key` varchar(128) DEFAULT NULL COMMENT '配置KEY',
  `option_value` text COMMENT '配置内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='配置信息表，用来保存网站的所有配置信息。';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_option`
--

LOCK TABLES `jp_option` WRITE;
/*!40000 ALTER TABLE `jp_option` DISABLE KEYS */;
INSERT INTO `jp_option` VALUES (1,'web_config','http://www.jplus.cc');
/*!40000 ALTER TABLE `jp_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_role`
--

DROP TABLE IF EXISTS `jp_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '序号',
  `pid` int(11) DEFAULT NULL COMMENT '父角色id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `deptid` int(11) DEFAULT NULL COMMENT '部门名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '保留字段(暂时没用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_role`
--

LOCK TABLES `jp_role` WRITE;
/*!40000 ALTER TABLE `jp_role` DISABLE KEYS */;
INSERT INTO `jp_role` VALUES (1,1,0,'超级管理员',24,'admin',1),(5,2,1,'临时',26,'temp',NULL);
/*!40000 ALTER TABLE `jp_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_role_menu`
--

DROP TABLE IF EXISTS `jp_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menuid` int(11) DEFAULT NULL COMMENT '菜单id',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3681 DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_role_menu`
--

LOCK TABLES `jp_role_menu` WRITE;
/*!40000 ALTER TABLE `jp_role_menu` DISABLE KEYS */;
INSERT INTO `jp_role_menu` VALUES (3377,105,5),(3378,106,5),(3379,107,5),(3380,108,5),(3381,109,5),(3382,110,5),(3383,111,5),(3384,112,5),(3385,113,5),(3386,114,5),(3387,115,5),(3388,116,5),(3389,117,5),(3390,118,5),(3391,119,5),(3392,120,5),(3393,121,5),(3394,122,5),(3395,150,5),(3396,151,5),(3624,105,1),(3625,106,1),(3626,107,1),(3627,108,1),(3628,109,1),(3629,110,1),(3630,111,1),(3631,112,1),(3632,113,1),(3633,165,1),(3634,166,1),(3635,167,1),(3636,114,1),(3637,115,1),(3638,116,1),(3639,117,1),(3640,118,1),(3641,162,1),(3642,163,1),(3643,164,1),(3644,119,1),(3645,120,1),(3646,121,1),(3647,122,1),(3648,150,1),(3649,151,1),(3650,128,1),(3651,134,1),(3652,158,1),(3653,159,1),(3654,130,1),(3655,131,1),(3656,135,1),(3657,136,1),(3658,137,1),(3659,152,1),(3660,153,1),(3661,154,1),(3662,132,1),(3663,138,1),(3664,139,1),(3665,140,1),(3666,155,1),(3667,156,1),(3668,157,1),(3669,133,1),(3670,160,1),(3671,161,1),(3672,141,1),(3673,142,1),(3674,143,1),(3675,144,1),(3676,148,1),(3677,145,1),(3678,149,1),(3679,168,1),(3680,169,1);
/*!40000 ALTER TABLE `jp_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_user`
--

DROP TABLE IF EXISTS `jp_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(45) DEFAULT NULL COMMENT '账号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) DEFAULT NULL COMMENT 'md5密码盐',
  `name` varchar(45) DEFAULT NULL COMMENT '名字',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) DEFAULT NULL COMMENT '电话',
  `deptid` int(11) DEFAULT NULL COMMENT '部门id',
  `status` int(11) DEFAULT NULL COMMENT '状态(1：启用  2：冻结  3：删除）',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `version` int(11) DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='管理员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_user`
--

LOCK TABLES `jp_user` WRITE;
/*!40000 ALTER TABLE `jp_user` DISABLE KEYS */;
INSERT INTO `jp_user` VALUES (1,'girl.gif','admin','10015a3b1d8aa7e21d69aae77ff7b59e778fd994592969cbd7550a0d8da222c6','91b27d02f093fe1de1','张三','2017-05-05 00:00:00',2,'sn93@qq.com','18200000000',24,1,'2016-01-29 08:49:53',25),(44,NULL,'test','10015a3b1d8aa7e21d69aae77ff7b59e778fd994592969cbd7550a0d8da222c6','91b27d02f093fe1de1','test','2017-05-01 00:00:00',1,'abc@123.com','',26,1,'2017-05-16 20:33:37',NULL);
/*!40000 ALTER TABLE `jp_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jp_user_role`
--

DROP TABLE IF EXISTS `jp_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jp_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户和角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jp_user_role`
--

LOCK TABLES `jp_user_role` WRITE;
/*!40000 ALTER TABLE `jp_user_role` DISABLE KEYS */;
INSERT INTO `jp_user_role` VALUES (1,1,1);
/*!40000 ALTER TABLE `jp_user_role` ENABLE KEYS */;
UNLOCK TABLES;


