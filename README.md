# Jplus后台管理系统

## 简介
这是一个基于**JBoot**实现的微服务后台管理系统脚手架。

目前实现了框架的基本搭建

项目将持续更新，尽请期待。。。

## 模块介绍
模块名称 | 模块介绍
---|---
jplus-admin  |后台管理模块
jplus-common |公共模块，工具类、常量 
jplus-config |统一配置模快
jplus-core   |核心模块
jplus-facade |service 接口层 
jplus-service|service 实现层
jplus-wechat |微信管理模块 


## 项目地址
- 开源中国：https://gitee.com/retire/jplus


> 感谢jboot、guns、Kisso、H+。本后端基于jboot架构，前端基于H+;

## 运行方法
1. 使用maven将项目导入到IDE中
2. 将docs下的sql导入数据库
3. 修改jplus-config中的jboot.properties的配置文件路径，可配置到docs目录下
4. 启动jplus-config
5. 启动jplus-service
6. 启动jplus-admin
7. 访问http://localhost:8080

## 注意
- Jplus需要redis与zookeeper支持

## 生成jar包方法
使用maven打包**窗口**运行程序:

```
clean package appassembler:assemble
```

使用maven打包**后台**运行程序:

```
clean package appassembler:generate-daemons
```
## SSO配置,Jplus集成Kisso
添加sso.properties配置文件,详细见Kisso文档

文档URL地址 ： [点击这里](https://gitee.com/baomidou/kisso/attach_files)
```properties
################ SSOConfig file #################
#test_mode,online_mode
sso.run.mode=test_mode

sso.secretkey=Kisso4JfinalK80mAS
sso.cookie.name=uid
sso.cookie.domain=192.168.1.6
# Default RC4 , You can choose [ DES , AES , BLOWFISH , RC2 , RC4 ]
#sso.encrypt.algorithm=BLOWFISH
sso.token.class=io.jplus.core.plugin.kisso.JplusToken
sso.login.url=http://192.168.1.6:8080/admin/login
#sso.cache.class=com.baomidou.kisso.TestCache
```
修改shiro.ini
```ini
myRealm = io.jplus.core.plugin.shiro.SSOAuthRealm
```

> 后台登录帐号:admin  
> 后台登录密码:111111

### 静态图展示
![登录](docs/images/login.png)
![后台主页](docs/images/index.png)
![列表页](docs/images/list.png)
![编辑页](docs/images/edit.png)
![统一配置中心](docs/images/config.png)
![swagger](docs/images/swagger.png)
![hystrix](docs/images/hystrix.png)
