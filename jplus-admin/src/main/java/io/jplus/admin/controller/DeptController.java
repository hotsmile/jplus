/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.paragetter.Para;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.StrKit;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.Consts;
import io.jplus.admin.model.Dept;
import io.jplus.admin.service.DeptService;
import io.jplus.core.base.BaseController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping(value = "/admin/dept", viewPath = Consts.BASE_VIEW_PATH+"admin/dept")
public class DeptController extends BaseController {

    @JbootrpcService
    DeptService deptService;


    public void list() {
        Columns columns = Columns.create();
        String condition = getPara("condition");
        if (StrKit.notBlank(condition)) {
            columns.like("fullname", condition);
        }
        List<Dept> deptList = deptService.findByColumn(columns);
        renderJson(deptList);
    }

    public void edit(Integer deptId) {
        Dept dept = new Dept();

        if (deptId != null && deptId.intValue() > 0) {
            dept = deptService.findById(deptId);
            deptService.join(dept,"pid","pidName");
            dept.put("pidName", deptService.findById(dept.getPid()).getFullname());
        }

        setAttr("dept", dept);
        render("edit.html");
    }

    @Before(POST.class)
    public void save(@Para("dept")Dept dept) {
        boolean tag = deptService.saveOrUpdate(dept);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("保存失败！");
        }
    }

    public void tree() {
        List<Dept> deptList = deptService.findAll();
        renderJson(buildTree(deptList).toArray());
    }

    /**
     * 构建部门树
     *
     * @param deptList
     * @return
     */
    private List<Map<String, Object>> buildTree(List<Dept> deptList) {
        List<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();

        for (Dept dept : deptList) {
            Map map = new HashMap();
            map.put("id", dept.getId());
            map.put("name", dept.getFullname());
            map.put("pid", dept.getPid());
            map.put("open", true);
            map.put("check", false);
            treeList.add(map);
        }
        return treeList;
    }

    public void delete(String deptId) {
        boolean tag = deptService.deleteById(deptId);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("删除失败！");
        }
    }


}
