/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.interceptor;

import io.jboot.Jboot;
import io.jboot.utils.EncryptCookieUtils;
import io.jboot.utils.StringUtils;
import io.jboot.web.fixedinterceptor.FixedInterceptor;
import io.jboot.web.fixedinterceptor.FixedInvocation;
import io.jplus.Consts;
import io.jplus.admin.model.User;
import io.jplus.admin.service.UserService;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Package io.jplus.admin.interceptor
 */
public class JplusUserInteceptor implements FixedInterceptor {

    UserService userService = Jboot.service(UserService.class);

    @Override
    public void intercept(FixedInvocation inv) {

        String userId = EncryptCookieUtils.get(inv.getController(), Consts.JPLUS_USER_ID);
        if (StringUtils.isNotBlank(userId)) {
            User user = userService.findById(userId);
            if (user != null) {
                inv.getController().setAttr(Consts.JPLUS_USER, user);
            }
        }

        inv.invoke();
    }
}
